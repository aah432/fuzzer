# Amber Harding
# SWEN 331
# Fuzzer

import mechanicalsoup
import argparse
import requests
from bs4 import BeautifulSoup
import requests


def main():
    # Connect to DVWA
    url = "http://localhost"
    browser = mechanicalsoup.StatefulBrowser(user_agent='MechanicalSoup')
    browser.open(url)

    # Find all links using the CSS selector
    for link in browser.page.select('a'):
        print(link.text)

    form = browser.select_form()
    browser.get_current_form().print_summary()
    browser['username'] = 'admin'
    browser['password'] = 'password'
    browser.submit_selected()
    browser.launch_browser()
    browser.follow_link()
    print(browser.get_current_page)


def parser():
    # create top level parser
    disc_parser = argparse.ArgumentParser(prog='Discovery Options')
    test_parser = argparse.ArgumentParser(prog='Test Options')

    disc_parser.add_argument('--discover', action=discover, help='Output a comprehensive, human-readable list of all discovered inputs to the system. Techniques include both crawling and guessing.')
    test_parser.add_argument('--test')

    disc_parser.parse_args(['--discover', 'http://localhost:8080'])


class discover(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(discover, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, disc_parser, namespace, values, option_string=None):
        def __call__(self, parser, args, values, option_string=None):
            print("boi")
            setattr(args, self.dest, values)

        return discover

    def crawl(self):
        browser = mechanicalsoup.StatefulBrowser()
        browser.open(argparse)


if __name__ == '__main__':
    main()
